// 获取当前选中的文本
function getSelectedText() {
    let selection = window.getSelection();
    if (selection && selection.rangeCount && selection.toString()) {
        return selection.toString();
    }
    return null;
}

// 发送翻译请求
function translate(word, event) {
    const appid = 'APP_ID'; // 替换成自己的 APP ID
    const key = 'APP_SECRET'; // 替换成自己的密钥
    const salt = Date.now().toString();
    const sign = md5(appid + word + salt + key); // 使用 md5 加密生成签名
    const url = `https://fanyi-api.baidu.com/api/trans/vip/translate?q=${encodeURIComponent(word)}&from=en&to=zh&appid=${appid}&salt=${salt}&sign=${sign}`;

    // 向扩展工具发送消息
    chrome.runtime.sendMessage({
            type: 'get',
            url: url
        },
        response => {
            const translationText = response.trans_result[0].dst; // 获取翻译结果
            addTooltip(translationText, event); // 在页面上添加提示框
            // return translationText;
        }
    );

}

// 在页面上添加提示框
function addTooltip(text, event) {
    const tooltip = document.createElement('div');
    tooltip.innerHTML = text;
    tooltip.style.cssText = `
    position: absolute;
    top: 0;
    left: 0;
    z-index: 99999;
    padding: 0.5rem;
    font-size: 1rem;
    background-color: #fff;
    border: 1px solid #aaa;
    box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
  `;
    document.body.append(tooltip);
    // 计算提示框位置
    tooltip.style.top = event.pageY + 'px';
    tooltip.style.left = event.pageX + 'px';
    // 添加点击事件，点击以后自动关闭提示框
    tooltip.addEventListener('click', () => tooltip.remove());
}

// 添加鼠标选中事件监听器
document.addEventListener('mousedown', async (event) => {
    // 如果选中文本为空，则不继续执行
    const selectedText = getSelectedText();
    console.log(selectedText);
    if (!selectedText) {
        return;
    }
    // 发送翻译请求
    const translation = await translate(selectedText, event);
    // console.log(translation);
    // addTooltip("translation", event); // 在页面上添加提示框
});

function isWord(text) {
    return /^[a-zA-Z]+$/.test(text);
}